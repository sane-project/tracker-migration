// parseItemFollowupsTab.js
// Copyright 2018  Olaf Meeuwissen
//
// License: AGPL-3.0+

'use strict'

const cheerio = require('cheerio')

module.exports = (tabBody) => {
  const $ = cheerio.load(tabBody)
  const rv = []
  $('.listing td').each(function (i, elem) {
    const entry = {}
    entry.date = $(this).html().split('<br>')[0]
      .replace('Date:', '').trim()

    entry.name = $(this).html().split('<br>')[1]
      .replace('Sender:', '').trim()
    if (entry.name.includes('href=')) {
      entry.name = $(this).children('a').first().text()
      entry.user = $(this).children('a').first().attr('href')
        .split('/')[4]
    }
    entry.text = $(this).text()
      .replace('Date: ' + entry.date, '')
      .replace('Sender: ' + entry.name, '').trim()

    rv.push(entry)
  })
  return rv
}
