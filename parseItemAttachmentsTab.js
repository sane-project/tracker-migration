// parseItemAttachmentsTab.js
// Copyright 2018  Olaf Meeuwissen
//
// License: AGPL-3.0+

'use strict'

const cheerio = require('cheerio')

module.exports = (tabBody) => {
  const $ = cheerio.load(tabBody)
  const rv = []
  $('.listing tbody tr').each(function (i, elem) {
    const entry = {}
    entry.size = $(this).children('td').eq(0).text()
    entry.name = $(this).children('td').eq(1).text()
    entry.date = $(this).children('td').eq(2).text()
    entry.user = $(this).children('td').eq(3).text()
    entry.link = $(this).children('td').eq(4).html()
      .replace('<a href="', '')
      .replace('">' + $(this).children('td').eq(1).html() + '</a>', '')
    rv.push(entry)
  })
  return rv
}
