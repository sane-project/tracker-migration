// fetchIndexPages.js -- from first to last
// Copyright 2018  Olaf Meeuwissen
//
// License: AGPL-3.0+

'use strict'

const config = require('config')
const path = require('path')

const cache = require('./cache')
const fetch = require('./fetch')

const parseTrackerIndex = require('./parseTrackerIndex')
const fetchTrackerItems = require('./fetchTrackerItems')

module.exports = fetchIndexPages

function fetchIndexPages (tracker, options) {
  const format = config.fetch.format || '0000'
  const start = options.qs.start
  const length = Math.max(format.length,
    start.toString().length)
  const file = path.join(config.fetch.cache,
    tracker.group_id,
    tracker.atid,
    'index',
    (format + start).slice(-length) + '.html')

  fetch(options, file, body => {
    const index = parseTrackerIndex(body)

    cache(JSON.stringify(index, null, 2),
      file.replace(/\.html$/, '.json'))

    fetchTrackerItems(tracker, index)
    if (index.total !== index.last) {
      options.qs.start = index.last
      fetchIndexPages(tracker, options)
    }
  })
}
