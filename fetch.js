// fetch.js -- potentially cached HTML content
// Copyright 2018  Olaf Meeuwissen
//
// License: AGPL-3.0+

'use strict'

const logger = require('./logger')
const config = require('config')

const Limiter = require('limiter').RateLimiter
const throttle = new Limiter(config.fetch.count, config.fetch.interval)
const queue = []

const fs = require('fs')
const util = require('util')
const read = util.promisify(fs.readFile)

module.exports = (src, dst, fun) => {
  logger.debug('fetching', dst)
  read(dst)
    .then(body => {
      logger.debug('using cache for', dst)
      fun(body)
    })
    .catch(err => {
      if (err.code !== 'ENOENT') throw err
      logger.info('scheduling', dst, 'for download')
      schedule({src: src, dst: dst, fun: fun})
      throttle.removeTokens(1, drainQueue)
    })
}

function schedule (req) {
  (req.dst.includes('/index/') // prioritize
    ? queue.unshift(req)
    : queue.push(req))
}

const cache = require('./cache')
const request = require('request-promise-native')

function drainQueue (err, remainingRequests) {
  if (err) throw err

  if (queue.length) {
    const req = queue.shift()
    logger.info('downloading', req.dst)
    request(req.src)
      .then(body => {
        logger.debug('downloaded', req.dst)
        cache(body, req.dst)
        req.fun(body)
      })
      .catch(err => {
        logger.error(' downloading', req.dst, '(' + err.error.code + ')')
      })
  }
}
