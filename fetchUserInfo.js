// fetchUserInfo.js -- that we do not already have
// Copyright 2018  Olaf Meeuwissen
//
// License: AGPL-3.0+

'use strict'

const config = require('config')
const path = require('path')

const cache = require('./cache')
const fetch = require('./fetch')

const parseUserInfo = require('./parseUserInfo')

const users = new Set()

module.exports = (tracker, user) => {
  if (!user) return
  if (user === 'None') return
  if (user === 'Nobody') return
  if (users.has(user)) return

  users.add(user)
  const options = {
    baseUrl: config.fetch.forge,
    uri: [config.fetch.users, user].join('/')
  }
  const file = path.join(config.fetch.cache,
    tracker.group_id,
    'users',
    user + '.html')

  fetch(options, file, body => {
    const rv = parseUserInfo(body, user)

    if (rv) {
      cache(JSON.stringify(rv, null, 2),
        file.replace(/\.html$/, '.json'))
    }
  })
}
