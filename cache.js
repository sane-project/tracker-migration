// cache.js -- downloaded "stuff" to file
// Copyright 2018  Olaf Meeuwissen
//
// License: AGPL-3.0+

'use strict'

const fs = require('fs')
const path = require('path')
const util = require('util')
const mkdir = util.promisify(fs.mkdir)
const write = util.promisify(fs.writeFile)

module.exports = (stuff, file) => {
  write(file, stuff)
    .catch(err => {
      if (err.code !== 'ENOENT') throw err
      mkdirP(path.dirname(file))
        .then(() => { write(file, stuff) })
    })
}

async function mkdirP (dir) {
  const parent = path.dirname(dir)
  if (parent !== dir) { await mkdirP(parent) }
  await mkdir(dir)
    .catch(err => { if (err.code !== 'EEXIST') throw err })
}
