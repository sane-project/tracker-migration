// fetchTrackerItems.js -- content, attachments and user info
// Copyright 2018  Olaf Meeuwissen
//
// License: AGPL-3.0+

'use strict'

const logger = require('./logger')
const config = require('config')
const path = require('path')
const megasize = require('filesize').partial({standard: 'iec'})
const kilosize = require('filesize')
      .partial({standard: 'iec', round: 0, symbols: {B: 'bytes'}})

const cache = require('./cache')
const fetch = require('./fetch')

const parseTrackerItem = require('./parseTrackerItem')
const fetchUserInfo = require('./fetchUserInfo')

module.exports = (tracker, index) => {
  index.items.forEach(item => {
    const options = {
      baseUrl: config.fetch.forge,
      uri: item.link
    }
    const file = path.join(config.fetch.cache,
      tracker.group_id,
      tracker.atid,
      item.aid + '.html')

    fetch(options, file, body => {
      const rv = parseTrackerItem(body)

      cache(JSON.stringify(rv, null, 2),
        file.replace(/\.html$/, '.json'))

      rv.attachments.items.forEach(attachment => {
        const options = {
          baseUrl: config.fetch.forge,
          uri: attachment.link,
          encoding: null // pull attachments as binary
        }
        const dir = attachment.link.split('/').reverse()[1]
        const file = path.join(config.fetch.cache,
          tracker.group_id,
          tracker.atid,
          item.aid,
          dir,
          attachment.name)
        fetch(options, file, (data) => {
          if (kilosize(data.length) == attachment.size) return
          if (megasize(data.length) == attachment.size) return
          logger.warn('size mismatch for', attachment.name,
                      ',', megasize(data.length),
                      '!=', attachment.size)
        })
      })

      // Collect info on the users involved in this tracker item
      if (rv.submitted_to) fetchUserInfo(tracker, rv.submitted_to.user)
      if (rv.assigned_to) fetchUserInfo(tracker, rv.assigned_to.user)
      const tabs = [
        rv.followups,
        rv.attachments,
        rv.changes
      ]
      tabs.forEach(elem => {
        elem.items.forEach(item => {
          fetchUserInfo(tracker, item.user)
        })
      })
    })
  })
}
