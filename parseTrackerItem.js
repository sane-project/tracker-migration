// parseItem.js -- fields and tabs
// Copyright 2018  Olaf Meeuwissen
//
// License: AGPL-3.0+

'use strict'

const logger = require('./logger')
const cheerio = require('cheerio')

module.exports = (body) => {
  const $ = cheerio.load(body)
  const rv = {}

  // This is not strictly necessary but convenient to have embedded
  // in the item's information for easy of reference later.
  rv._group_id = $('input[name=group_id]').attr('value')
  rv._atid = $('input[name=atid]').attr('value')
  const header = $('#maindiv h1').text()
  rv._aid = header.match(/\[#\d+\]/)[0].match(/\d+/)[0]
  rv._title = header.substr(header.indexOf(' ')).trim()

  // Populate the fields.  The exact content depends on the tracker's
  // configuration but this code is expected to be pretty generic and
  // handle all fields correctly.
  $('td', '#maindiv table[border]').each(function (i, elem) {
    const text = $(this).text().trim()

    if (!text) return // empty td elements

    if (text.startsWith('Detailed description')) {
      // The "Detailed description" is the last entry but it is in a
      // nested td element, so we return false to prevent processing
      // it twice.
      rv['details'] = text.replace('Detailed description', '').trim()
      return false
    }

    // Remaining fields are colon separated.

    const colon = text.indexOf(':')
    let field = text.substring(0, colon).trim()
    let value = text.substring(colon + 1).trim()

    if (field === 'Submitted by' || field === 'Assigned to') {
      field = field.replace(/ /g, '_').toLowerCase()
      rv[field] = { name: value }
      if (rv[field].name.endsWith(')')) {
        rv[field].user = rv[field].name.match(/\([^)]*\)$/)[0]
          .replace(/[()]/g, '').trim()
        rv[field].name = rv[field].name
          .replace('(' + rv[field].user + ')', '').trim()
      }
    } else {
      field = field.replace(/ /g, '_').toLowerCase()
      if (field === 'state') field = 'status_id'
      rv[field] = value
    }
  })

  // Process the tabbed widgets.
  const cb = {
    followups: require('./parseItemFollowupsTab'),
    attachments: require('./parseItemAttachmentsTab'),
    commits: require('./parseItemCommitsTab'),
    changes: require('./parseItemChangesTab')
  }
  $('.tabbertab', '#maindiv div#tabber').each(function (i, elem) {
    let field = $(this).attr('title')
    let count = 0
    if (field.endsWith(')')) {
      count = field.match(/\([^)]*\)$/)[0].replace(/[()]/g, '').trim()
      field = field.replace('(' + count + ')', '').trim()
    }
    field = field.replace(/ /g, '_').toLowerCase()
    rv[field] = { count: count }
    if (cb[field]) {
      rv[field].items = cb[field]($(this).html())
    } else {
      logger.warn('Ignoring unknown tab:', field)
    }
  })
  return rv
}
