// pull.js -- all tracker related information for a project
// Copyright 2018  Olaf Meeuwissen
//
// License: AGPL-3.0+

'use strict'

const logger = require('./logger')
const config = require('config')

const path = require('path')
const url = require('url')

const cache = require('./cache')
const fetch = require('./fetch')

const fetchIndexPages = require('./fetchIndexPages')
const parseSummaryTab = require('./parseSummaryTab')
const parseTrackerTab = require('./parseTrackerTab')

if (process.argv.length < 3) {
  logger.error('usage: npm run pull group_id\n' +
            '   or: node pull.js group_id')
  process.exit(1)
}

if (process.argv.length > 3) {
  const ignored = process.argv
    .filter((e, i) => { return i > 2 })
    .map(e => { return '"' + e + '"' })
    .join(' ')
  logger.warn('ignoring additional arguments:', ignored)
}

let groupID = process.argv[2]
if (isNaN(groupID)) {
  logger.error('expected numeric group_id, got', groupID)
  process.exit(1)
}

const options = {
  baseUrl: config.fetch.forge,
  uri: config.fetch.tracker,
  qs: { group_id: groupID }
}
const file = path.join(config.fetch.cache, groupID, 'trackers.html')

fetch(options, file, body => {
  const project = parseTrackerTab(body)
  project.group_id = groupID

  const options = {
    baseUrl: config.fetch.forge,
    uri: project.link,
    headers: {
      Accept: 'text/html'
    }
  }
  const file = path.join(config.fetch.cache, groupID, 'summary.html')

  fetch(options, file, body => {
    const rv = parseSummaryTab(body)
    project.desc = rv.desc
    project.tags = rv.tags
    project.trove = rv.trove
    project.since = rv.since

    cache(JSON.stringify(project, null, 2),
      path.join(config.fetch.cache, groupID, 'project.json'))
  })

  project.trackers.forEach(tracker => {
    const link = url.parse(tracker.link, true)
    const options = {
      baseUrl: config.fetch.forge,
      uri: link.pathname,
      qs: link.query
    }
    options.qs.set = 'custom'
    options.qs._assigned_to = 0 // anyone
    options.qs._status = 100 // any
    options.qs._sort_col = 'artifact_id'
    options.qs._sort_ord = 'ASC'
    options.qs.start = 0

    tracker.group_id = groupID

    fetchIndexPages(tracker, options)
  })
})
