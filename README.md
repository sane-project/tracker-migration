![AGPL-3.0+](https://www.gnu.org/graphics/agplv3-88x31.png)

SANE Project Alioth Tracker Item Migration
==========================================

Pulling Trackers
----------------

Look up the `group_id` for the tracker you want to migrate.  You can
find this in the tracker's URL.  Then run

``` sh
npm install --production
npm run pull 30186              # group_id
```

The above uses the value that is appropriate for the SANE project.
Adjust for your project(s) as needed.

This will fetch:

 - the project's Tracker and Summary tabs
 - indices for all trackers on the Tracker tab
 - all tracker items, open as well as closed, for all trackers
 - any attachments present for all tracker items
 - user pages for all users that submitted, were assigned to, added
   followups to, attached files to and/or changed items on any item

and at the same time extract information deemed relevant for the
migration in JSON format.

The HTML pages that have been fetched and corresponding JSON extracts
are cached to disk as in the file system tree layout below (using the
SANE project as an example).

```
 30186                         # group_id for the project
 ├── 410366                    # atid of the Bugs tracker
 │   ├── 300103.html           # tracker item HTML page, aid + '.html'
 │   ├── 300103.json           # tracker item JSON extract
 ...
 │   ├── 300118                # aid attachments
 │   │   └── 22
 │   │       └── sane-v4l-update.tar.gz
 │   ├── 300118.html
 │   ├── 300118.json
 ...
 │   └── index                 # index pages
 │       ├── 0000.html
 │       ├── 0000.json
 ...
 ├── 410369                    # atid of the Feature Requests tracker
 ...                           # (same layout as above)
 ├── project.json              # JSON extract of the following two files
 ├── summary.html              # Summary tab
 ├── trackers.html             # Tracker tab
 └── users                     # user pages
     ...
     ├── olaf-guest.html
     ├── olaf-guest.json
     ...
```

The main thing to note is that attachments to a tracker item are not
simply tracked by name alone because there may be multiple attachments
with the same name.  Instead, there is one extra level of indirection
between the tracker item's `aid` and the attachment itself.  The name
for that level is the number that is embedded in the attachment's URL.

Pushing Tracker Items
---------------------

This is still on the agenda.
