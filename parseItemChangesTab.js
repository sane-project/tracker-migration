// parseItemChangesTab.js
// Copyright 2018  Olaf Meeuwissen
//
// License: AGPL-3.0+

'use strict'

const cheerio = require('cheerio')

module.exports = (tabBody) => {
  const $ = cheerio.load(tabBody)
  const rv = []
  $('.listing tbody tr').each(function (i, row) {
    const cols = $(this).children('td')
    const entry = {
      field: $(cols).eq(0).text().replace(/ /g, '_').toLowerCase(),
      old_value: $(cols).eq(1).text(),
      date: $(cols).eq(2).text(),
      user: $(cols).eq(3).text()
    }
    rv.push(entry)
  })
  return rv
}
