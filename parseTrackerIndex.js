// parseTrackerIndex.js -- to get a list of tracker items
// Copyright 2018  Olaf Meeuwissen
//
// License: AGPL-3.0+

'use strict'

const cheerio = require('cheerio')

module.exports = (body) => {
  const $ = cheerio.load(body)

  const [first, last, total] = $('p', '#maindiv').next()
    .text().match(/\d+/g)

  const items = []
  $('tbody tr', '.listing').each(function (i, elem) {
    const col = $(this).children('td').first()
    const rv = {
      aid: $(col).text(),
      link: $(col).children('a').attr('href')
    }
    items.push(rv)
  })

  return {
    first: first,
    last: last,
    total: total,
    items: items
  }
}
